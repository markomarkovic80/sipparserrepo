# SIP Parser

SIP is a signalling protocol very often used for Voice over IP.

Implement a basic SIP parser that will pass all the tests. The public API is already
present in the `lib/` folder (as a facade only). The message body doesn't have to be parsed and
can be maintained as a whole.

## Prerequisites

In order to run the application you need to have elixir installed on your system.You can find installation instructions here - [https://elixir-lang.org/install.html](https://elixir-lang.org/install.html)

When elixir is installed go to the terminal and cd into the `SIP_PARSER` folder.

When in the folder run `mix deps get` in your terminal. This command is going to fetch application dependencies.
Optionally you can read more about how elixir applications function by following introduction guide - [https://elixir-lang.org/getting-started/introduction.html](https://elixir-lang.org/getting-started/introduction.html)

## What to do

When you are done with previous steps run `mix test` from the same folder in the terminal. The initial result should return `3 errors out 4 tests` -

```bash
  1) test can parse a SIP INVITE (SIPParserTest)
     test/sip_parser_test.exs:6
     Refute with == failed, both sides are exactly equal
     code: refute message.start_line == nil
     left: nil
     stacktrace:
       test/sip_parser_test.exs:10: (test)



  2) test can get userpart of the request uri (SIPParserTest)
     test/sip_parser_test.exs:15
     Assertion with == failed
     code:  assert request_user == "bob"
     left:  "user"
     right: "bob"
     stacktrace:
       test/sip_parser_test.exs:19: (test)



  3) test SIP INFO method is allowed (SIPParserTest)
     test/sip_parser_test.exs:22
     Expected truthy, got false
     code: assert allowed
     stacktrace:
       test/sip_parser_test.exs:27: (test)



Finished in 0.06 seconds
4 tests, 3 failures
```

Your task is to make changes in the application code for those tests can pass.

## Hints

Hints are here to help you save time and succeed with the task.

### Hint 1 Where do to start first

Parser attempts to parse the sample of SIP request that is stored in `priv/SIP_INVITE.txt`. Start by looking at the SIP contents of the request.

### Hint 2 Where to put changes

First step of a parsing process should start in `sip_parser.ex` files inside `parse` function -

```elixir
@spec parse(binary()) :: {:ok, t} | {:error, term()}
  def parse(_sip_message) do
    {:ok, %__MODULE__{}}
  end

```

Rest of the work will be completed in two subsequent functions `get_request_user(_parsed_sip_message)` and `allowed?(_parsed_sip_message, _method)`.

### Hint 3 Walk through introduction guide

When new to the language please go to the elixir [introduction guide](https://elixir-lang.org/getting-started/introduction.html) before making changes to the code. It is a great place to start and learn basics of the language needed to complete this task.

Good luck!

