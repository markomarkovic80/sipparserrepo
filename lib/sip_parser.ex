defmodule SIPParser do
  @moduledoc """
  The SIPParses module takes a SIP request and parses it to a native data structure and can do
  basic operations on the SIP message.
  """
  @type t :: %__MODULE__{
          headers: term(),
          body: binary(),
          start_line: term()
        }
  defstruct [:headers, :body, :start_line]

  @doc """
  Parses a raw SIP request to the internal representation.
  """
  @spec parse(binary()) :: {:ok, t} | {:error, term()}
  def parse(sip_message) do
    [start_line_s | _headers_body] = String.split(sip_message, "\n")
    [start_line_headers_s, body_s] = String.split(sip_message, "\r\n\r\n")
    headers_s = remove_prefix(start_line_headers_s, start_line_s <> "\n")
    {:ok, %__MODULE__{start_line: start_line_s, headers: headers_s, body: body_s}}
  end

  @doc """
  Gets the userpart from the URI which is present in the Request-Line. The Request-Line is the
  first line of the SIP request.
  """
  @spec get_request_user(t) :: binary()
  def get_request_user(parsed_sip_message) do
    [_, user_server, _] = String.split(parsed_sip_message.start_line, ":")
    [user, _] = String.split(user_server, "@")
    user
  end

  @doc """
  Confirms if a certain SIP method is allowed or not for this User-Agent. This is signalled by the
  User-Agent in the `Allow`-header.
  """
  @spec allowed?(t, binary()) :: boolean()
  def allowed?(parsed_sip_message, method) do
    headers_list = String.split(parsed_sip_message.headers, "\n")
    allow_header = Enum.find(headers_list, fn x -> x=~"Allow:" end)
    [_, methods_with_commas] = String.split(allow_header, ":")
    methods = String.split(String.trim(methods_with_commas), ", ")
    Enum.member?(methods, method)
  end

  defp remove_prefix(full, prefix) do
    base = byte_size(prefix)
    <<_::binary-size(base), rest::binary>> = full
    rest
  end

end
